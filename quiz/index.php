<?php include "../header.php" ?>
<?php

    // Initialize first load. 
    if(!isset($_SESSION['user'])){

        // Create the initial account.
        $_SESSION['user']['score']    = 0;
        $_SESSION['user']['questionNumber'] = 0;

        // Load the randomized 10 set of questions.
        $conn = new mysqli("localhost", "root", "", "ynsdb");

        // Check the connection.
        if(!$conn){
            echo "An error occurred while trying to connect to the question bank.";
            die();
        }

        $_SESSION['user']['questions'] = array();

        // Load the questions.
        $questions = $conn->query("SELECT * FROM questions ORDER BY RAND()");
        while($question = $questions->fetch_assoc()){
            array_push($_SESSION['user']['questions'], $question);
        }

        $conn->close();
        
    }


  
    // Fires when a post has been sent to this page.
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        
        if(isset($_POST['quizAnswer'])){
            if($_POST['quizAnswer'] !=""){
                if($_POST['quizAnswer'] == $_SESSION['user']['questions'][$_SESSION['user']['questionNumber']]["answer"]){
                    
                    // If correct answer add to score.
                    $_SESSION['user']['score']++;
        
                }

                // Increment next question.
                if($_SESSION['user']['questionNumber'] < count($_SESSION['user']['questions'])){
                    $_SESSION['user']['questionNumber']++;
                }
            }
        }
        
        //echo "Previous Answer: ". $_POST['quizAnswer']."<br>";
    }
    
    //echo "Score: ".$_SESSION['user']['score']."<br>";
    //echo "Number: ".$_SESSION['user']['questionNumber']."<br>";
?>
<link href="<?= BASE_DIR ?>css/quiz.css" rel="stylesheet"/>

<?php if($_SESSION['user']['questionNumber'] == count($_SESSION['user']['questions'])):?>
    <div class="container">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">You got <?= $_SESSION['user']['score'] ?> points!</h1>
                <p class="lead">
                    <?php
                        if($_SESSION['user']['score'] == 0){
                            echo "Well, that's embarassing.";
                        }
                        if($_SESSION['user']['score'] >= 1 && $_SESSION['user']['score'] <= 3){
                            echo "Somehow bad. Use Google.";
                        }
                        if($_SESSION['user']['score'] >= 4 && $_SESSION['user']['score'] <= 5){
                            echo "Not bad for a 50 percent average! ";
                        }
                        if($_SESSION['user']['score'] >= 6 && $_SESSION['user']['score'] <= 8){
                            echo "You are a natural born gamer.";
                        }
                        if($_SESSION['user']['score'] >= 9 && $_SESSION['user']['score'] <= 10){
                            echo "Cheater! nah, just kidding. Congratulations! Session reset! Try again.";
                        }

                        unset($_SESSION['user'])
           

                    ?>
                </p>
                <div class="text-right">
                    <a class="btn btn-success btn-lg" 
                        href="<?= BASE_DIR ?>quiz/index.php" 
                        role="button">
                        <i class="fas fa-arrow-left"></i> Try Again
                    </a>
                    <a class="btn btn-success btn-lg" 
                        href="<?= BASE_DIR ?>exercises.php" 
                        role="button">
                        Quit <i class="fas fa-arrow-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php return;?>
<?php endif;?>
<div class="container">
<div class="container-fluid">
    <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
        
            <h3><span class="label label-warning" id="qid"><?= $_SESSION['user']['questionNumber']+1 ?>.)</span> 
                <?= $_SESSION['user']['questions'][$_SESSION['user']['questionNumber']]["question"]?> 
            </h3>
        </div>
        <div class="modal-body">
          <div class="quiz" id="quiz" data-toggle="buttons">
           <?php

                // Dynamic load the choices per question with randomization.
                $conn = new mysqli("localhost", "root", "", "ynsdb");

                // Check the connection.
                if(!$conn){
                    echo "An error occurred while trying to load the choices.";
                    die();
                }
        
                $choices = array();
                $question_id = $_SESSION['user']['questions'][$_SESSION['user']['questionNumber']]['id'];

                // Load the questions.
                $result = $conn->query("SELECT * FROM choices 
                WHERE question_id=$question_id ORDER BY RAND()");

                while($choice = $result->fetch_assoc()){
                    array_push($choices, $choice);
                }
        
                $conn->close();
                
           ?>
            <form id="answerSheet" method="POST" action="index.php">
           <?php foreach($choices as $choice):?>
         
                <label class="element-animation1 res btn btn-lg btn-primary btn-block">
                <span class="btn-label"><i class="fas fa-arrow-right"></i></span> 
                <input type="radio" value="<?= $choice['value'] ?>">
                    <?= $choice['value'] ?>
                </label>
         
           <?php endforeach;?>
           </form>
           </div>
   </div>
   <div class="modal-footer text-muted">
    <span id="answer"></span>
</div>
</div>
</div>
</div>
</div>
<script>
$(document).ready(function(){
    var loading = $('#loadbar').hide();
    $(".res").on('click',function () {
        var choice = $(this).find('input:radio').val();
        const form = document.createElement('form');
        form.method = "POST";
        form.action = "index.php";
     
        const hiddenField = document.createElement('input');
        hiddenField.type = 'hidden';
        hiddenField.name = "quizAnswer";
        hiddenField.value = choice;
        form.appendChild(hiddenField);
      
        document.body.appendChild(form);
        form.submit();
    });	
});


</script>
<?php include "../footer.php" ?>