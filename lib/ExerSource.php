<?php 

    # ----------------------------------------------------------------------------------------------------------------
    # ExerSource (Exercises Source) is a custom class built 
    # to switch out from CSV to DATABASE data source on the fly.
    # ----------------------------------------------------------------------------------------------------------------
    # USAGE: $mySource = new ExerSource(TYPE)

    // Configure this if using MySQL Database connection.
    define("HOST","localhost");
    define("USERNAME", "root");
    define("PASSWORD", "");
    define("DATABASE_NAME", "ynsdb");

  
   

    class ExerSource {
        private $connectionType;
        private $numberOfUsers;
        private $users = array();
        private $csvPath = "../data/users.csv";

        private $columnNames = array(
            "id",
            "first_name",
            "last_name",
            "email_address",
            "password",
            "photoFileName"
        );

        function __construct($type, $filePath=null) {
            
            if($filePath){
                $this->csvPath = $filePath;
            }

            if($type == "CSV"){

                $handle = null;

                if(!file_exists($this->csvPath)){

                    // Open the CSV file for read and write.
                    $handle = fopen($this->csvPath, "w+");


                }else{
                    
                    // Open the CSV file for read only.
                    $handle = fopen($this->csvPath, "r");
                    
                }


                // Load the CSV file. 
                while($user = fgetcsv($handle, 1000, ",")){
                    array_push($this->users, $user);
                }

                // Fill the number of users variable.
                $this->numberOfUsers = count($this->users);

                // Close the file handle.
                fclose($handle);

                $this->connectionType = $type;
            
            }elseif($type == "DB"){
                $conn = new mysqli(HOST, USERNAME, PASSWORD, DATABASE_NAME);
                
                // Check initial connection.
                if(!$conn){
                    throw new Exception("Failed to connect to the database.");
                    return null;
                }

                $this->users = $conn->query("SELECT * FROM users")->fetch_assoc();
                $this->numberOfUsers = count($this->users);
                $this->connectionType = $type;

            }else{
                return null;
            }
        }
        public function setUserData($id, $index, $value){

            if($this->connectionType == "DB"){
                $conn = new mysqli(HOST, USERNAME, PASSWORD, DATABASE_NAME);
                
                if(!$conn){
                    throw new Exception("Cannot update user since the connection to database cannot be established.");
                    return null;
                }
                
                $field = $this->columnNames[$index];
                $conn->query("UPDATE users SET $field = '$value' WHERE id=$id");
                return;

            }

            if(!file_exists($this->csvPath)){
                throw new Exception("The file does not exist to rewrite.");
                return null;
            }

            $temp = $this->getUsers();
            $i = 0;
            foreach($temp as $d){
                if($d[0] == $id){
                    $temp[$i][$index] = $value;
                }
                $i++;
            }

            unlink($this->csvPath);
            $csvFile = fopen($this->csvPath, "a");

            foreach($temp as $data){
                fputcsv($csvFile, $data);
            }
            
            fclose($csvFile);
        }
        public function deleteUser($id){ 
            if($this->connectionType == "DB"){

                $conn = new mysqli(HOST, USERNAME, PASSWORD, DATABASE_NAME);
                
                if(!$conn){
                    throw new Exception("Cannot insert new user since the connection to database cannot be established.");
                    return null;
                }
                
                $conn->query("DELETE FROM users WHERE id=$id");
                return;

            }

            if(!file_exists($this->csvPath)){
                throw new Exception("The file does not exist to delete a user.");
                return null;
            }

            $temp = $this->getUsers();
            $i = 0;
            foreach($temp as $d){
                if($d[0] == $id){
                    unset($temp[$i]);
                }
                $i++;
            }

            unlink($this->csvPath);
            $csvFile = fopen($this->csvPath, "a");

            foreach($temp as $data){
                fputcsv($csvFile, $data);
            }
            
            fclose($csvFile);
        }
        public function updateUserPhoto($id, $fileName){
            return $this->setUserData($id, 5, $fileName);
        }
        public function getUsers($count=null, $startIndex=0){

            if($this->connectionType == "DB"){
                $conn = new mysqli(HOST, USERNAME, PASSWORD, DATABASE_NAME);
                
                if(!$conn){
                    throw new Exception("Failed to connect to the database.");
                    return null;
                }

                $this->users = array();
              
                if($count){

                    $result = $conn->query("SELECT * FROM users LIMIT $startIndex, $count ");

                    while($user = $result->fetch_array(MYSQLI_NUM)){
                        //echo $user["id"]. " - ".$user["first_name"]. " - ".$user["last_name"]. " - ".$user["email_address"]. " - ".$user["password"]. " - ".$user["photoFileName"]."<br>";
                        array_push($this->users, $user);
                    }

                }else{

     
                    $result = $conn->query("SELECT * FROM users");

                    while($user = $result->fetch_array(MYSQLI_NUM)){
                        //echo $user["id"]. " - ".$user["first_name"]. " - ".$user["last_name"]. " - ".$user["email_address"]. " - ".$user["password"]. " - ".$user["photoFileName"]."<br>";
                        array_push($this->users, $user);
                    }

                }

                $this->numberOfUsers = count($this->users);
                return $this->users;
              
            }
            if($this->numberOfUsers == 0){
                return null;
            }

            $users = array();
            $c = $this->numberOfUsers;
            $i = $startIndex;

            if($count != null){
                $c = $count;
            }

            if($startIndex > ($this->numberOfUsers - 1)){
                throw new Exception("The startIndex specified was out of bounds of the array.");
                return null;
            }

            while($c > 0){

                if(empty($this->users[$i])){
                    break;
                }

                array_push($users, $this->users[$i]);
                $c--; 
                $i++;
            }

            
            return $users;
        }
        public function getUser($id,$index=null){
            for($i=0 ;$i < $this->getCount(); $i++ ){
                if($this->users[$i][0] == $id){ 
                    if($index){
                        return $this->users[$i][$index];
                    }else{
                        return array(
                            $this->users[$i][0],
                            $this->users[$i][1],
                            $this->users[$i][2],
                            $this->users[$i][3],
                            $this->users[$i][4],
                            $this->users[$i][5]
                        );
                    }

                }
            }
        }
        public function insertUser($userData){

            if($this->connectionType == "DB"){
                $conn = new mysqli(HOST, USERNAME, PASSWORD, DATABASE_NAME);
                
                if(!$conn){
                    throw new Exception("Cannot insert new user since the connection to database cannot be established.");
                    return null;
                }
                
                $conn->query("INSERT INTO users (first_name, last_name, email_address, password, photoFileName)
                VALUES('$userData[1]', '$userData[2]', '$userData[3]', '$userData[4]', '$userData[5]')");

                return;
            }

            $csvFile = fopen($this->csvPath, "a");
            fputcsv($csvFile, $userData);
            fclose($csvFile);
        }
        public function getCount(){
            return $this->numberOfUsers;
        }

        public function loginUser($emailAddress, $password){
            if($this->numberOfUsers == 0){
                return false;
            }
            $users = $this->getUsers();
            foreach($users as $user){
                if($user[3] == $emailAddress){
                    if(password_verify($password, $user[4])){
                        $_SESSION['USER_ID'] = $user[0];
                        $_SESSION['USER_NAME'] = $user[1].' '.$user[2];
                        $_SESSION['USER_EMAIL_ADDRESS'] = $user[3];
                        $_SESSION['USER_PHOTO'] = $user[5];
                        return true;
                    }
                }
            }
            return false;
        }


    }
?>