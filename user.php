<?php
    include 'header.php';
    // Check the file CSV.
    if(!file_exists('data/users.csv')){

        // If there is no users yet (no file), make the user create one first. 
        header("Location: ". BASE_DIR. "exercises/1-6.php");
        exit();

    }

    // Check if there is already a logged in user_id.
    if(!isset($_SESSION['USER_ID'])){

        // Make user log in first to access this page.
        header("Location: login.php");
        exit();
        
    }

    // Check the user_id if valid. 
    if($_SESSION['USER_ID'] == ""){
        
        // Make user log in first to access this page.
        header("Location: login.php");
        exit();

    }
?>

<div class="container">
    <div class="card mt-4">
        <div class="card-body text-center"> 
            <div style="width:200px;height:auto;margin:auto;">
            <?php if($_SESSION['USER_PHOTO'] == "default-profile"):?>
                <img src="img/default-profile.svg" class="rounded float-center" style="width:auto;height:100%" alt="A photo of <?=$_SESSION['USER_NAME']?>">
            <?php else:?>
                <img src="data/photos/<?= $_SESSION['USER_PHOTO'] ?>" class="rounded float-center" style="width:100%;height:auto" alt="A photo of <?=$_SESSION['USER_NAME']?>">
            <?php endif;?>    
            </div>
            <h4 class="mt-4"><?= $_SESSION['USER_NAME']?></h4>
            <h6><?= $_SESSION['USER_EMAIL_ADDRESS']?></h6>
            <form method="POST" action="<?= BASE_DIR ?>upload.php" name="uploaderForm" enctype="multipart/form-data">
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" onchange="document.uploaderForm.submit()" name="fileToUpload" required>
                    <label class="custom-file-label" for="fileToUpload">Click Browse to choose and upload profile photo.</label>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" name="uploadProfilePhoto" type="submit">Upload</button>
                </div>
            </div>
            </form>

        </div>
    </div>
</div>

<?php include 'footer.php' ?>