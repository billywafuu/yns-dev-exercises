<?php include "../header.php" ?>
<div class="container mt-4">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Image File Name</h1>
            <p class="lead">Show alert when you click an image. Alert will tell you file name of the image.</p>
            <div class="text-right">
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-6.php" 
                    role="button">
                    <i class="fas fa-arrow-left"></i> Prev
                </a>
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-8.php" 
                    role="button">
                    Next <i class="fas fa-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="container text-center" >
        <img id="imgContainer" class="img-fluid" src="../img/logo-red.png">
    </div>
</div>
<?php include "../footer.php" ?>
<script>
    // On load of the DOM, hook a function to the image.
    window.onload = () => {
        var imgContainer = g("imgContainer");
        imgContainer.onclick = () => {
            alert(
                
                // Alerts the file name by replacing and removing
                // the URL parts keeping the end file name.
                (imgContainer.src).replace(/^.*[\\\/]/,'')

            );
        }
    }
</script>