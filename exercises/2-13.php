<?php include "../header.php" ?>
<div class="container mt-4">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Image Resizer</h1>
            <p class="lead">Create 3 buttons below an image. These buttons' label has "Small", "Medium" and "Large" text. 
            If you press any button, the size of the image will be transformed.</p>
            <div class="text-right">
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-12.php" 
                    role="button">
                    <i class="fas fa-arrow-left"></i> Prev
                </a>
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-14.php" 
                    role="button">
                    Next <i class="fas fa-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="container text-center" >
        <img id="imgContainer" class="img-fluid" src="../img/logo-red.png">
        <div class="row">
            <div class="col-md-4 p-2">
                <button type="button" onclick="imageToSmall()" class="btn btn-primary btn-block"">Small</button>
            </div>
            <div class="col-md-4 p-2">
                <button type="button" onclick="imageToMedium()" class="btn btn-primary btn-block"">Medium</button>
            </div>
            <div class="col-md-4 p-2">
                <button type="button" onclick="imageToLarge()" class="btn btn-primary btn-block"">Large</button>
            </div>            
        </div>       
  </div>
    </div>
</div>
<?php include "../footer.php" ?>
<script>
    var imgContainer;
    window.onload = function(){
        imgContainer = g("imgContainer");
    }
    function imageToSmall(){
        imgContainer.style.width = "150px";
        imgContainer.style.height = "100px"; 
    }
    function imageToMedium(){
        imgContainer.style.width = "250px";
        imgContainer.style.height = "200px";        
    }
    function imageToLarge(){
        imgContainer.style.width = "350px";
        imgContainer.style.height = "300px"; 
    }
</script>