<?php include "../header.php" ?>
<div class="container mt-4">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">The four basic operations of arithmetic</h1>
            <p class="lead">Create 2 text box, 1 combo box and 1 submit button in order to input expressions and choose the way to calculate plus, minus, multiply, division.</p>
            <div class="text-right">
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-2.php" 
                    role="button">
                    <i class="fas fa-arrow-left"></i> Prev
                </a>
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-4.php" 
                    role="button">
                    Next <i class="fas fa-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="form-group">
            <label for="inputFirstNumber">Enter first number</label>
            <input 
                type="number" 
                class="form-control" 
                id="inputFirstNumber" 
                value="0" 
                placeholder="(e.g 250)"
            >
        </div>
        <div class="form-group">
            <label for="inputSecondNumber">Enter second number</label>
            <input 
                type="number" 
                class="form-control" 
                id="inputSecondNumber" 
                value="0" 
                placeholder="(e.g 100)"
            >
        </div> 
        <label for="inputOperation">Choose an operation:</label>
        <select class="form-control" id="inputOperation">
            <option value="addition">+</option>
            <option value="subtraction">-</option>
            <option value="multiplication">x</option>
            <option value="division">/</option>
        </select>
        <button type="button" 
            class="btn btn-primary btn-block mt-2"
            onclick="calculate()"
        >
            Calculate
        </button>
    </div>
</div>
<?php include "../footer.php" ?>
<script>
    function calculate(){
        var selectedOperation = g("inputOperation").value;
        if(selectedOperation == "addition"){
            alert("The sum is: " + (Number(g("inputFirstNumber").value) + Number(g("inputSecondNumber").value)));
        }else if(selectedOperation == "subtraction"){
            alert("The difference is: " + (Number(g("inputFirstNumber").value) - Number(g("inputSecondNumber").value)));
        }else if(selectedOperation == "multiplication"){
            alert("The product is: " + (Number(g("inputFirstNumber").value) * Number(g("inputSecondNumber").value)));
        }else if(selectedOperation == "division"){
            alert("The quotient is: " + (Number(g("inputFirstNumber").value) / Number(g("inputSecondNumber").value)));
        }else{
            alert("Please select an operator from the box.");
        }
    }
</script>