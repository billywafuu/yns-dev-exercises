<?php include "../header.php" ?>
<div class="container mt-4">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Confirm dialog and redirection</h1>
            <p class="lead">Create a page with a button. When you click the button, show confirm dialog.</p>
            <div class="text-right">
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-1.php" 
                    role="button">
                    <i class="fas fa-arrow-left"></i> Prev
                </a>
            </div>
        </div>
    </div>
    <div class="container text-center">
        <button class="btn btn-success btn-lg"
            role="button"
            onclick="confirmRedirection()">
            Click Me to Redirect
        </button>
    </div>
</div>
<?php include "../footer.php" ?>
<script>
    function confirmRedirection() {
        if(confirm("Are you sure you want to move on to the next page?")){
            window.location = "<?= BASE_DIR ?>exercises/2-3.php";
        }
    }
</script>