<?php include "../header.php" ?>
    <?php   

        // Unset any previous information posted. 
        unset($_SESSION['USER_INFO']);
        
        // A message string. 
        $errorMessages = array();

        if($_SERVER['REQUEST_METHOD'] === 'POST'){

            // Check POST objects so we can prevent logging of PHP errors when a
            // client post undefined objects.
            if(
                !isset($_POST['inputFirstName'])      ||
                !isset($_POST['inputLastName'])       ||
                !isset($_POST['inputEmailAddress'])   ||
                !isset($_POST['inputPassword'])       ||
                !isset($_POST['inputRepeatPassword'])){
                
                // If either the of objects is not set on POST, just simply redirect the page.
                header("Location: ". BASE_DIR ."exercises/1-6.php");
                exit();

            }

            // Server side validation. We can secure field requirements on the client
            // side but its good to have a secondary validation on the server side just incase.
            if($_POST['inputFirstName'] == "" || $_POST['inputLastName']  == ""){
                
                // Push a message that will be rendered on the DOM.
                array_push($errorMessages,"Your first name and last name is required.");

            }


            // Perform validation check on the email address.
            if (!filter_var($_POST['inputEmailAddress'], FILTER_VALIDATE_EMAIL)) {

                // Push a message that will be rendered on the DOM if email address has incorrect format.
                array_push($errorMessages,"Please enter a valid email address. (e.g juandelacruz@yahoo.com)");

            }

            // Perform check on the password string length rule. The minimum length is 8 for better security.
            if( strlen($_POST['inputPassword']) < 8 || strlen($_POST['inputRepeatPassword']) < 8){

                // Push a message that will be rendered on the DOM if password has less than 8 characters.
                array_push($errorMessages,"Please enter a password that has 8 characters and above.");

            }

            // Perform check on the password and the repeat password match. 
            if($_POST['inputPassword'] != $_POST['inputRepeatPassword']){
                
                // Push a message that will be rendered on the DOM if password and the repeat password
                // does not match at all.
                array_push($errorMessages,"The password and the repeat password must match. Please re-enter.");

            }

            // When no errors.
            if(count($errorMessages) == 0){


                // Proceed to insert the data to the $_SESSION.
                $_SESSION['USER_INFO'] = $_POST;
                
                // Redirect to the display page for confirmation on saving into CSV file.
                header("Location: ". BASE_DIR ."views/information.php");
                exit();

            }

        }
    ?>
    <div class="container mt-4">
    <div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Input User Information</h1>
        <p class="lead">
            Create a form for basic user information. When you enter values in each input then press the submit button, 
            it will show the inputted data in another page.
        </p>
        <div class="text-right">
            <a class="btn btn-success btn-lg" 
                href="<?= BASE_DIR ?>exercises/1-5.php" 
                role="button">
                <i class="fas fa-arrow-left"></i> Prev
            </a>
        </div>
    </div>
    </div>
        <div class="card" style=" width: 80%; margin:auto">
            <div class="card-body">
                <h5 class="card-title">User Information</h5>
                <h6 class="card-subtitle mb-2 text-muted">Enter your information.</h6>
                <?php if(count($errorMessages)> 0):?>
                        <div class="alert alert-warning" role="alert">
                        Please resolve the following errors:<br>
                        <?php 
                            foreach($errorMessages as $message){
                                echo "* ". $message;
                            }
                        ?>
                        </div>
                    <?php endif;?>
                <form method="POST" action="<?= BASE_DIR ?>exercises/1-6.php">
                    <div class="form-group">
                        <input type="text" class="form-control" name="inputFirstName" aria-describedby="emailHelp" placeholder="First Name" required
                        value="<?= isset($_POST['inputFirstName']) ? $_POST['inputFirstName'] : '' ?>">
                        <small class="form-text text-muted">Enter your first name.</small>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="inputLastName" aria-describedby="emailHelp" placeholder="Last Name" required
                        value="<?= isset($_POST['inputLastName']) ? $_POST['inputLastName'] : '' ?>">
                        <small class="form-text text-muted">Enter your last name.</small>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="inputEmailAddress" aria-describedby="emailHelp" placeholder="Last Name" required
                        value="<?= isset($_POST['inputEmailAddress']) ? $_POST['inputEmailAddress'] : '' ?>">
                        <small class="form-text text-muted">Enter your email address.</small>
                    </div>
                    <div class="form-group">
                        <input type="password" minlength="8" class="form-control" name="inputPassword" aria-describedby="emailHelp" placeholder="Password" required
                        value="<?= isset($_POST['inputPassword']) ? $_POST['inputPassword'] : '' ?>">
                        <small class="form-text text-muted">Enter your desired password.</small>
                    </div>
                    <div class="form-group">
                        <input type="password" minlength="8" class="form-control" name="inputRepeatPassword" placeholder="Password" required
                        value="<?= isset($_POST['inputRepeatPassword']) ? $_POST['inputRepeatPassword'] : '' ?>">
                        <small class="form-text text-muted">Repeat your password.</small>
                    </div>
                   
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="inputCheckbox" required>
                        <label class="form-check-label" for="inputCheckbox">I agree to the Terms of Service.</label>
                    </div>
                <button type="submit" class="btn btn-primary mt-2">Submit</button>
                </form>
            </div>
        </div>
    </div>
<?php include("../footer.php"); ?>