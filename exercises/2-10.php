<?php include "../header.php" ?>
<div class="row">
    <div class="col-12 p-2">
        <button type="button" onclick="scrollToBottom()" class="btn btn-secondary btn-block""><i class="fas fa-arrow-down"></i> Scroll To Bottom</button>
    </div> 
</div>
<div class="container mt-4" style="height:2000px">
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">The Scroller</h1>
    <p class="lead">Create a page that is atleast two times the screen height. 
    There are 2 buttons located at the top of the page and at the bottom of the page. 
    When you click top button, screen will scroll down to the bottom. Vice versa.
    </p>
    <div class="text-right">
        <a class="btn btn-success btn-lg" 
            href="<?= BASE_DIR ?>exercises/2-9.php" 
            role="button">
            <i class="fas fa-arrow-left"></i> Prev
        </a>
        <a class="btn btn-success btn-lg" 
            href="<?= BASE_DIR ?>exercises/2-11.php" 
            role="button">
            Next <i class="fas fa-arrow-right"></i>
        </a>
    </div>
  </div>
</div>
  <?php for($i = 0; $i < 100; $i++):?>
    <p>Create a page that is atleast two times the screen height. 
    There are 2 buttons located at the top of the page and at the bottom of the page. 
    When you click top button, screen will scroll down to the bottom. Vice versa.
    </p>
  <?php endfor; ?>

  <div class="row position-relative absolute-bottom">
    <div class="col-12 p-2">
        <button type="button" onclick="scrollToTop()" class="btn btn-secondary btn-block"">Scroll To Top <i class="fas fa-arrow-up"></i></button>
    </div>      
  </div>
</div>
<?php include "../footer.php" ?>
<script>
    function scrollToTop(){
        window.scrollTo(0, 0);
    }
    function scrollToBottom(){
        window.scrollTo(0, document.documentElement.scrollHeight);
    }
</script>

