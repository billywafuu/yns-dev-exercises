<?php include "../header.php" ?>
<div class="container mt-4">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Label Generator</h1>
            <p class="lead">Whenever you press a button, new label will be appended below the button.</p>
            <div class="text-right">
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-5.php" 
                    role="button">
                    <i class="fas fa-arrow-left"></i> Prev
                </a>
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-7.php" 
                    role="button">
                    Next <i class="fas fa-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="container" id="mainContainer">
        <button type="button" 
            class="btn btn-primary btn-block mt-2"
            onclick="createNewLabel()"
        >
            Create New Label +
        </button>
        <label>This is label 1.</label>
        <br>
    </div>
</div>
<?php include "../footer.php" ?>
<script>

    // Prepare variables.
    var numberOfLabels = 1;
    var mainContainer;

    // On load of the DOM, get the mainContainer for reference.
    window.onload = () => {
        mainContainer = g("mainContainer");
    }

    function createNewLabel(){
        var newLabel = document.createElement("Label");
        numberOfLabels++;
        newLabel.innerHTML = "This is label " + numberOfLabels;
        mainContainer.append(newLabel);
        mainContainer.append(document.createElement("Br"));
    }
</script>