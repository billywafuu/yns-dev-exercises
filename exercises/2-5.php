<?php include "../header.php" ?>
<div class="container mt-4">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Reactive text box and label</h1>
            <p class="lead">Whenever you type a character in a text box, text in a label will be updated what you typed.</p>
            <div class="text-right">
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-4.php" 
                    role="button">
                    <i class="fas fa-arrow-left"></i> Prev
                </a>
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-6.php" 
                    role="button">
                    Next <i class="fas fa-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="form-group">
            <label for="inputText" id="lblReactiveLabel"></label>
            <input 
                type="text" 
                class="form-control" 
                id="inputText" 
                placeholder="Enter any text"
            >
        </div>

    </div>
</div>
<?php include "../footer.php" ?>
<script>
    window.onload = function(){

        // Reference the instance of the label and the input.
        var reactiveLabel = g("lblReactiveLabel");
        var inputText = g("inputText");

        // Hook an event to the inputText.
        inputText.onkeyup = () => {
            reactiveLabel.innerHTML = inputText.value;
        }

    }
</script>