<?php include "../header.php" ?>
<div class="container mt-4">
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Input Date</h1>
    <p class="lead">
      Show 3 days from inputted date and its day of the week. Display 1 text box in a page. 
      If you enter date in the text box and press the submit button, it will calculate and show the result.
    </p>
    <div class="text-right">
        <a class="btn btn-success btn-lg" 
            href="<?= BASE_DIR ?>exercises/1-4.php" 
            role="button">
            <i class="fas fa-arrow-left"></i> Prev
        </a>
        <a class="btn btn-success btn-lg" 
            href="<?= BASE_DIR ?>exercises/1-6.php" 
            role="button">
            Next <i class="fas fa-arrow-right"></i>
        </a>
    </div>
  </div>
</div>
<form method="POST" action="<?= BASE_DIR ?>exercises/1-5.php">
  <div class="form-group">
    <label for="dateInput">Select a date</label>
    <div class="form-group row">
      <div class="col-12">
        <input class="form-control" type="date" value="2020-10-19" name="dateInput">
      </div>
    </div>
  </div>
  <button type="submit" name="btnCalculate" class="btn btn-primary">Submit</button>
  <a id="result"/>
  <?php if ($_SERVER['REQUEST_METHOD'] === 'POST'): ?>
    <div class="alert alert-primary mt-4" role="alert">
        <?php 

          if(!isset($_POST['dateInput'])){
            echo "Please input valid numbers.";
            return;
          }

          date_default_timezone_set('Asia/Manila');
          $nextThreeDays = date("F j, Y", strtotime($_POST['dateInput']. ' + 3 days'));
          $dayOfTheWeek  = date("l", strtotime($nextThreeDays));
          echo "The date next 3 days is: ". $nextThreeDays . " which is ". $dayOfTheWeek;
          
        ?>
    </div>
  <?php endif;?>
  <br>
</form>
</div>

<?php include "../footer.php" ?>

