<?php include "../header.php" ?>
<div class="container mt-4">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4" id="lblDate"></h1>
            <p class="lead" id="lblTime"></p>
            <hr>
            <p class="lead" id="lblTime">Put a label in a page. That label shows date and time. 
            The label is refreshed every minute and show current date time..</p>            
            <div class="text-right">
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-14.php" 
                    role="button">
                    <i class="fas fa-arrow-left"></i> Prev
                </a>
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>logout.php" 
                    role="button">
                    Finish <i class="fas fa-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<?php include "../footer.php" ?>
<script>
    var currentDate = new Date();
    window.onload = function(){

        // Get the elements.
        var lblDate = g("lblDate");
        var lblTime = g("lblTime");

        var months = [
            "January", 
            "February", 
            "March", 
            "April", 
            "May", 
            "June", 
            "July", 
            "August", 
            "September", 
            "October", 
            "November", 
            "December"
        ];
        
        // Initialize date and time.
        lblDate.innerHTML =  `${months[currentDate.getMonth()]} ${currentDate.getDate()}, ${currentDate.getFullYear()}`;
        lblTime.innerHTML =  `${currentDate.getHours()}:${currentDate.getMinutes()}`;

        // Update every 5 second so it is 60 second accurate lol.
        setInterval(()=> {
            currentDate = new Date();
            lblDate.innerHTML =  `${months[currentDate.getMonth()]} ${currentDate.getDate()}, ${currentDate.getFullYear()}`;
            lblTime.innerHTML =  `${currentDate.getHours()}:${currentDate.getMinutes()}`;
        },
        5000);
    }
 
</script>