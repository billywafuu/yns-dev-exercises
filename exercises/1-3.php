<?php include "../header.php" ?>
<div class="container mt-4">
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Greatest Common Divisor</h1>
    <p class="lead">Display 2 text box in a page. If you enter numbers in the text box and press the submit button, it will calculate and show the result.</p>
    <div class="text-right">
        <a class="btn btn-success btn-lg" 
            href="<?= BASE_DIR ?>exercises/1-2.php" 
            role="button">
            <i class="fas fa-arrow-left"></i> Prev
        </a>
        <a class="btn btn-success btn-lg" 
            href="<?= BASE_DIR ?>exercises/1-4.php" 
            role="button">
            Next <i class="fas fa-arrow-right"></i>
        </a>
    </div>
  </div>
</div>
<form method="POST" action="<?= BASE_DIR ?>exercises/1-3.php">
  <div class="form-group">
    <label for="inputFirstNumber">Enter first number</label>
    <input 
        type="number" 
        class="form-control" 
        name="inputFirstNumber" 
        value="0" 
        placeholder="(e.g 250)"
    >
  </div>
  <div class="form-group">
    <label for="inputSecondNumber">Enter second number</label>
    <input 
        type="number" 
        class="form-control" 
        name="inputSecondNumber" 
        value="0" 
        placeholder="(e.g 100)"
    >
  </div>
  <button type="submit" name="btnCalculate" class="btn btn-primary">Submit</button>

  <a id="result"/>
  <?php if ($_SERVER['REQUEST_METHOD'] === 'POST'): ?>
    <div class="alert alert-primary mt-4" role="alert">
        <?php 

            if(!isset($_POST['inputFirstNumber']) || !isset($_POST['inputSecondNumber'])){
                echo "Please input valid numbers.";
                return;
            }

            $firstNumber    = $_POST['inputFirstNumber'];
            $secondNumber   = $_POST['inputSecondNumber'];

            echo "Their greatest common divisor: ". gmp_gcd($firstNumber, $secondNumber);

        ?>
    </div>
  <?php endif;?>
  <br>
</form>
</div>

<?php include "../footer.php" ?>

