<?php include "../header.php" ?>
<div class="container mt-4">
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Color Changer</h1>
    <p class="lead">Create some buttons that show name of color. When you click one of them, text or background color will be changed according to what you press.</p>
    <div class="text-right">
        <a class="btn btn-success btn-lg" 
            href="<?= BASE_DIR ?>exercises/2-8.php" 
            role="button">
            <i class="fas fa-arrow-left"></i> Prev
        </a>
        <a class="btn btn-success btn-lg" 
            href="<?= BASE_DIR ?>exercises/2-10.php" 
            role="button">
            Next <i class="fas fa-arrow-right"></i>
        </a>
    </div>
  </div>
</div>
  <label class="h6">Select Background Color</label>
  <br>
  <div class="row">
    <div class="col-md-3 p-2">
        <button type="button" onclick="changeBackgroundColor('Red')" class="btn btn-danger btn-block"">Red</button>
    </div>
    <div class="col-md-3 p-2">
        <button type="button" onclick="changeBackgroundColor('Green')" class="btn btn-success btn-block"">Green</button>
    </div>
    <div class="col-md-3 p-2">
        <button type="button" onclick="changeBackgroundColor('Blue')" class="btn btn-primary btn-block"">Blue</button>
    </div>
    <div class="col-md-3 p-2">
        <button type="button" onclick="changeBackgroundColor('Yellow')" class="btn btn-warning btn-block"">Yellow</button>
    </div>       
  </div>
</div>
<?php include "../footer.php" ?>
<script>
    function changeBackgroundColor(color){
        document.body.style.backgroundColor = color;
    }
</script>

