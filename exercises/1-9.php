<?php include "../header.php" ?>
<?php

   // Create a source object since this page will
   // use a data source. The data path only make sense when
   // using a CSV as source.
   $source = new ExerSource($defaultSource,'../data/users.csv');
   $usersToPrint = $source->getUsers();
 
    // The default page to be loaded if $_GET['page'] is not set.
    define("DEFAULT_PAGE", 1);

    // The max records that will be rendered per page.
    define("MAX_RECORDS_PER_PAGE", 5);

    // Default page. 
    $currentPage = DEFAULT_PAGE; 

    // Receive the page GET server request for paging.
    if(isset($_GET['page'])){
    
        // Check if its a number. 
        if(is_numeric($_GET['page'])){

            // Negative range check. 
            if($_GET['page'] < 0){

                // Floor to 1 instead. 
                $currentPage = 1;

            }else{
                $currentPage = $_GET['page'];
            }
          
        } 

    }

  


    // Check if CSV is empty. 
    if(empty($usersToPrint)){

        // Throw the user back to 1-6 to encode some user. 
        header("Location: ". BASE_DIR ."exercises/1-6.php");
        exit();

    }

    // If the CSV has users, count.
    $usersCount = count($usersToPrint);

    // Determine the number of pages.
    $numberOfPages = ceil($usersCount / MAX_RECORDS_PER_PAGE);

    // Determine if the currentPage is still valid. 
    if($currentPage < 0 || $currentPage > $numberOfPages){

        // Floor to 1. 
        $currentPage = 1;
        
    }

    // Determine the cursor load starting point. 
    $cursorPoint = ($currentPage-1) * MAX_RECORDS_PER_PAGE;

?>
<div class="container mt-4">
    <div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">User List</h1>
        <p class="lead">This page reads the the source selected from the upper right corner.</p>
        <div class="text-right">
            <a class="btn btn-success btn-lg" 
                href="<?= BASE_DIR ?>exercises/1-6.php" 
                role="button">
                <i class="fas fa-arrow-left"></i> Create User
            </a>
            <a class="btn btn-success btn-lg" 
                href="<?= BASE_DIR ?>login.php" 
                role="button">
                Login to Upload Photos <i class="fas fa-arrow-right"></i>
            </a>
        </div>
    </div>
    </div>
    
    <table class="table">
    <thead>
        <tr>
            <td>User ID</td>
            <td></td>
            <td>Full Name</td>
            <td>Email Address</td>
        </tr>
    </thead>
    <tbody>
        <?php

            // Set up the counter. 
            $counter = 0; 

        ?>
        <?php while($counter < MAX_RECORDS_PER_PAGE):?>
            <?php 
                if(empty($usersToPrint[$cursorPoint])){
                    break;
                }    
            ?>
            <tr>
                <td><?= $usersToPrint[$cursorPoint][0] ?></td>
                <td>
                    <div style="width:100px;height:100px;margin:auto;">
                    <?php if($usersToPrint[$cursorPoint][5] == "default-profile"):?>
                        <img src="../img/default-profile.svg" 
                            class="rounded" 
                            style="width:auto;height:100%" 
                    >
                    <?php else:?>
                        <img src="../data/photos/<?= $usersToPrint[$cursorPoint][5] ?>" 
                            class="rounded" 
                            style="width:100%;height:auto" 
                        >
                    <?php endif;?>  
                    </div> 
                </td>
                <td> <?= $usersToPrint[$cursorPoint][1] ?> <?= $usersToPrint[$cursorPoint][2] ?> </td>
                <td><?= $usersToPrint[$cursorPoint][3]?></td>                                
            </tr> 
            <?php 

                // Increment counter and cursor.
                $counter++;
                $cursorPoint++;  

            ?>
        <?php endwhile; ?>

        <?php 

            // Free CSV. (it might be large but not at this point we know.)
            unset($usersToPrint);

        ?>
    </tbody>
    </table>
    <nav aria-label="Page navigation">
    <ul class="pagination">
        <li class   ="page-item">
            <a class="page-link" href="<?= BASE_DIR ?>exercises/1-9.php?page=<?= $currentPage > 1 ? $currentPage - 1 : $currentPage ; ?>">
            Previous
            </a>
        </li>
        <?php for($i = 1; $i <= $numberOfPages; $i++):?>
            <li class="page-item">
                <a class="page-link" href="<?= BASE_DIR ?>exercises/1-9.php?page=<?= $i ?>"> <?= $i ?></a>
            </li>       
        <?php endfor ?>
        <li class="page-item">
            <a class="page-link" href="<?= BASE_DIR ?>exercises/1-9.php?page=<?= $currentPage < $numberOfPages ? $currentPage + 1 : $numberOfPages ; ?>">
            Next
            </a>
        </li>
    </ul>
    </nav>
</div>
<?php include "../footer.php" ?>