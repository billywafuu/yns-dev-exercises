<?php include "../header.php"?>
<?php 
    
    // Include the calendar class.
    require_once "../lib/Calendar.php";



?>
<link rel="stylesheet" href="../css/calendar.css" >
<div class="container">
<?php 

    $calendar = new Calendar();
    echo $calendar->show();

?>

</div>
<?php include "../footer.php" ?>