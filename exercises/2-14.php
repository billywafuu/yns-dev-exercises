<?php include "../header.php" ?>
<div class="container mt-4">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Image Select</h1>
            <p class="lead">Prepare several images. Show one of them in a page and put a combo box beside the image. 
            When you choose one of the options in combo box, the image will be replaced with other one.</p>
            <div class="text-right">
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-13.php" 
                    role="button">
                    <i class="fas fa-arrow-left"></i> Prev
                </a>
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-15.php" 
                    role="button">
                    Next <i class="fas fa-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="container text-center" >
        <img id="imgContainer" class="img-fluid" src="../img/logo-red.png">  
        <div class="form-group">
            <label>Select a game</label>
            <select multiple class="form-control" id="selectImages" onchange="setCurrentImage(this.value)">
                <option value="cod-bo.jpg">Call of Duty: Black Ops</option>
                <option value="cod-aw.jpg">Call of Duty: Advanced Warfare</option>
                <option value="moh-wf.jpg">Medal of Honor: Warfighter</option>
            </select>
        </div>
    </div>
</div>
<?php include "../footer.php" ?>
<script>
    var imgContainer;
    window.onload = function(){
        imgContainer = g("imgContainer");
    }
    function setCurrentImage(name){
        imgContainer.src = "<?= BASE_DIR ?>img/" + name;
    }
</script>