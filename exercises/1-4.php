<?php include "../header.php" ?>
<div class="container mt-4">
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">FizzBuzz Problem</h1>
    <p class="lead">
      Display 1 text box in a page. If you enter number, which means last number, in the text box and press the submit button, it will calculate and show the result.
      Print each number from 1 up to submitted number on a new line. For each multiple of 3, print "Fizz" instead of the number. 
      For each multiple of 5, print "Buzz" instead of the number. For numbers which are multiples of both 3 and 5, print "FizzBuzz" 
    </p>
    <div class="text-right">
        <a class="btn btn-success btn-lg" 
            href="<?= BASE_DIR ?>exercises/1-3.php" 
            role="button">
            <i class="fas fa-arrow-left"></i> Prev
        </a>
        <a class="btn btn-success btn-lg" 
            href="<?= BASE_DIR ?>exercises/1-5.php" 
            role="button">
            Next <i class="fas fa-arrow-right"></i>
        </a>
    </div>
  </div>
</div>
<form method="POST" action="<?= BASE_DIR ?>exercises/1-4.php">
  <div class="form-group">
    <label for="inputNumber">Enter a number</label>
    <input 
        type="number" 
        class="form-control" 
        name="inputNumber" 
        value="0" 
        placeholder="Input number of lines that you want to print."
    >
  </div>
  <button type="submit" name="btnCalculate" class="btn btn-primary">Submit</button>

  <a id="result"/>
  <?php if ($_SERVER['REQUEST_METHOD'] === 'POST'): ?>
    <div class="alert alert-primary mt-4" role="alert">
        <?php 

          if(!isset($_POST['inputNumber'])){
            echo "Please input valid numbers.";
            return;
          }

          for($i = 1; $i <= $_POST['inputNumber']; $i++){

            if(($i % 3) == 0 && ($i % 5) == 0){
              echo "FizzBuzz<br>"; 
            }
            
            else if(($i % 3) == 0){
              echo "Fizz<br>"; 
            }
            
            else if(($i % 5) == 0){
              echo "Buzz<br>"; 
            }

            else{
              echo $i ."<br>";
            }
            
          }
          
            
        ?>
    </div>
  <?php endif;?>
  <br>
</form>
</div>

<?php include "../footer.php" ?>

