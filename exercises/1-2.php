<?php include "../header.php" ?>
<div class="container mt-4">
<form method="POST" action="<?= BASE_DIR ?>exercises/1-2.php#result">
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Four Basic Arithmetic</h1>
    <p class="lead">Display 2 text box in a page. If you enter numbers in the text box and press one of the submit buttons, it will calculate and show the result according to the operation.</p>
    <div class="text-right">
        <a class="btn btn-success btn-lg" 
            href="<?= BASE_DIR ?>exercises/1-1.php" 
            role="button">
            <i class="fas fa-arrow-left"></i> Prev
        </a>
        <a class="btn btn-success btn-lg" 
            href="<?= BASE_DIR ?>exercises/1-3.php" 
            role="button">
            Next <i class="fas fa-arrow-right"></i>
        </a>
    </div>
  </div>
</div>
  <div class="form-group">
    <label for="inputFirstNumber">Enter first number</label>
    <input 
        type="number" 
        class="form-control" 
        name="inputFirstNumber" 
        value="0" 
        placeholder="(e.g 250)"
    >
  </div>
  <div class="form-group">
    <label for="inputSecondNumber">Enter second number</label>
    <input 
        type="number" 
        class="form-control" 
        name="inputSecondNumber" 
        value="0" 
        placeholder="(e.g 100)"
    >
  </div>
  <label class="h6">Select Operator</label>
  <br>
  <div class="row">
    <div class="col-md-3 p-2">
        <button type="submit" name="btnAddition" class="btn btn-primary btn-block"">Addition</button>
    </div>
    <div class="col-md-3 p-2">
        <button type="submit" name="btnSubtraction" class="btn btn-primary btn-block"">Subtraction</button>
    </div>
    <div class="col-md-3 p-2">
        <button type="submit" name="btnMultiplication" class="btn btn-primary btn-block"">Multiplication</button>
    </div>
    <div class="col-md-3 p-2">
        <button type="submit" name="btnDivision" class="btn btn-primary btn-block"">Division</button>
    </div>       
  </div>
  <a id="result"/>
  <?php if ($_SERVER['REQUEST_METHOD'] === 'POST'): ?>
    <div class="alert alert-primary mt-4" role="alert">
        <?php 

            if(!isset($_POST['inputFirstNumber']) || !isset($_POST['inputSecondNumber'])){
                echo "Please input valid numbers.";
                return;
            }

            $firstNumber    = $_POST['inputFirstNumber'];
            $secondNumber   = $_POST['inputSecondNumber'];

            if(isset($_POST['btnAddition'])){
                echo "The sum is: ". round(($firstNumber + $secondNumber), 2);
            }
            else if(isset($_POST['btnSubtraction'])) {
                echo "The difference is: ". round(($firstNumber - $secondNumber),2);
            }
            else if(isset($_POST['btnMultiplication'])) {
                echo "The product is: ". round(($firstNumber * $secondNumber),2);
            }
            else if(isset($_POST['btnDivision'])) {

                // Instead of PHP returning Warning NaN, early check and bail out
                // of the operation when two given numbers are zeros.
                if($firstNumber == 0 && $secondNumber == 0){
                    echo "The quotient is undefined since both numbers are 0.";
                    return; 
                }

                echo "The quotient is: ". round(($firstNumber / $secondNumber), 2);
            }
            else {
                echo "Please select an operator from the selection.";
            }
        ?>
    </div>
  <?php endif;?>
  <br>
</form>

</div>

<?php include "../footer.php" ?>

