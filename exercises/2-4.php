<?php include "../header.php" ?>
<div class="container mt-4">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Show prime numbers</h1>
            <p class="lead">Create 1 text box and 1 submit button in order to display prime numbers from 2 to inputted number as maximum.</p>
            <div class="text-right">
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-3.php" 
                    role="button">
                    <i class="fas fa-arrow-left"></i> Prev
                </a>
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-5.php" 
                    role="button">
                    Next <i class="fas fa-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="form-group">
            <label for="inputMaxNumber">Enter max number</label>
            <input 
                type="number" 
                class="form-control" 
                id="inputMaxNumber" 
                value="0" 
                placeholder="Enter max number of prime numbers to display"
            >
        </div>
        <button type="button" 
      
            class="btn btn-primary btn-block mt-2"
            onclick="alertPrimeNumbers(g('inputMaxNumber').value)"
        >
            Show Prime Numbers
        </button>
    </div>
</div>
<?php include "../footer.php" ?>
<script>
    function alertPrimeNumbers(maxPrimeNumbers){
      if(maxPrimeNumbers == 0){
          alert("Please enter a number greater than 0.");
          return;
      }
      var primeNumbers = [];
      var counter = 0;
      var currentNumber = 2;


      while(counter < maxPrimeNumbers){
          if(currentNumber % 2 != 0  || currentNumber == 2){
            if(currentNumber % 3 != 0 || currentNumber == 3){
                if(currentNumber % 5 != 0 || currentNumber == 5){
                    if(currentNumber % 7 != 0 || currentNumber == 7){
                        if(currentNumber % 9 != 0 || currentNumber == 9){
                            primeNumbers.push(currentNumber);
                            counter++;
                        }
                    }
                }
            }
 
          }
          currentNumber++;
      }

      alert(`The ${maxPrimeNumbers} prime number(s) are ${primeNumbers}.`);

    }
</script>