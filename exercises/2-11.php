<?php include "../header.php" ?>

<div class="container mt-4">
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Background Color Animator</h1>
    <p class="lead">Try to implement animation in order to change background color from dark blue to light blue using JavaScript.
    </p>
    <div class="text-right">
        <a class="btn btn-success btn-lg" 
            href="<?= BASE_DIR ?>exercises/2-10.php" 
            role="button">
            <i class="fas fa-arrow-left"></i> Prev
        </a>
        <a class="btn btn-success btn-lg" 
            href="<?= BASE_DIR ?>exercises/2-12.php" 
            role="button">
            Next <i class="fas fa-arrow-right"></i>
        </a>
    </div>
  </div>
</div>
</div>
<?php include "../footer.php" ?>
<script>
  // On load of the DOM, run the function that will continously animate
  // when reaching two color ranges.
  document.body.style.backgroundColor = "rgba(	173, 216, 255, 1)";

  // Default 0 of the Red and Green.
  var r = 0;
  var g = 0;
  document.body.style.backgroundColor = `rgba(${r}, ${g}, 230, 1)`;

  window.onload = () => {
    setInterval(() => {

      // On every clock tick, randomize color increase of the r and g by 10s.

      if(r < 173){
        r += 10
      }else{
        r = 0;
      }
      if( g < 216){
        g += 10
      }else{
        g = 0;
      }
      // Mix it up with RGB.
      document.body.style.backgroundColor = `rgba(${r}, ${g}, 230, 1)`;
    }, 
    100);
    
  }
</script>

