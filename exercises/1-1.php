<?php include "../header.php" ?>
<div class="container mt-4">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Hello World!</h1>
            <p class="lead">Show Hello World! in a page.</p>
            <div class="text-right">
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>" 
                    role="button">
                    <i class="fas fa-arrow-left"></i> Home
                </a>
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/1-2.php" 
                    role="button">
                    Next <i class="fas fa-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<hr>
<br>
<?php include "../footer.php" ?>