<?php include "../header.php" ?>
<div class="container mt-4">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Image Hover Switch</h1>
            <p class="lead">Prepare 2 images. Then show 1 of them in a page. 
            When you mouse over the image, it will be changed with another image. 
            Furthermore you mouse out from the image, it will be changed with previous image.</p>
            <div class="text-right">
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-11.php" 
                    role="button">
                    <i class="fas fa-arrow-left"></i> Prev
                </a>
                <a class="btn btn-success btn-lg" 
                    href="<?= BASE_DIR ?>exercises/2-13.php" 
                    role="button">
                    Next <i class="fas fa-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="container text-center" >
        <img id="imgContainer" class="img-fluid" src="../img/logo-red.png">
    </div>
</div>
<?php include "../footer.php" ?>
<script>

    // Defaulth path src of image.
    var defaultSrc;

    // On load of the DOM, hook a function to the image.
    window.onload = () => {
        var imgContainer = g("imgContainer");
        defaultSrc = imgContainer.src;
        imgContainer.onmouseover = () => {
            imgContainer.src = "<?= BASE_DIR ?>img/default-profile.svg";
        }
        imgContainer.onmouseleave = () => {
            imgContainer.src = defaultSrc;
        }
    }
</script>