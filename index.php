<?php include 'header.php'?>
<div class="container mt-4">
  <div class="jumbotron">
    <h1 class="display-4">
      <img src="img/logo-red.png" width="100" height="60" class="d-inline-block align-top" alt="">
      YNS Dev Exercises
    </h1>
    <p class="lead">This small application contains all my refresher exercises course. Feels good to use native agaaiin! :)</p>
    <hr class="my-4">
    <p>This application uses native PHP Server Side Rendering, Bootstrap 4 and Font-Awesome.</p>
    <p class="lead">
      <a class="btn btn-primary btn-lg" 
          href="https://bitbucket.org/billywafuu/yns-dev-exercises/src/master/" 
          role="button">
          <i class="fab fa-bitbucket"></i> Bitbucket Repository
      </a>
      <a class="btn btn-secondary btn-lg" 
          href="https://docs.google.com/spreadsheets/d/1iXvZ0ba87WYRK5BjhmCZs7MI9H5wYeJxsGGc4QpUT5U/edit?ts=5bbc0eb9#gid=761364654" 
          role="button">
          <i class="fab fa-google"></i> Google Sheet Record
      </a>
      <a class="btn btn-danger btn-lg" href="<?= BASE_DIR ?>exercises/1-1.php" role="button">Start <i class="fas fa-play"></i></a>
    </p>
  </div>
  
</div>
<?php include 'footer.php'?>