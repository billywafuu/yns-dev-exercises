<?php
    include "header.php"; 
    
    // Check the file CSV.
    if(!file_exists('data/users.csv')){

        // If there is no users yet (no file), make the user create one first. 
        header("Location: ". BASE_DIR. "exercises/1-6.php");
        exit();

    }

    // Check if there is already a logged in user_id.
    if(isset($_SESSION['USER_ID'])){
        
        // If there is a logged in user_id, skip this login and proceed to user.php.
        header("Location: user.php");
        exit();

    }
    // Create a source object since this page will
    // use a data source. The data path only make sense when
    // using a CSV as source.
    $source = new ExerSource($defaultSource,'data/users.csv');
    echo "DEBUG: Number of users available in this source: " .$source->getCount();
    // Fires when user logs in.
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        
  
      
        if(!$source->loginUser($_POST['loginEmail'], $_POST['loginPassword'])){
            $_SESSION['PAGE_FLASHDATA'] = "invalid_login";
        }else{
            header("Location: user.php");
        }
    }
    

?>
    <div class="container">
        <div class="card mt-4">
            <div class="card-body">
                <h3>Log in as user</h3>
                <p>When you log in a user account, you will be able to upload or change its profile picture.</p>
                <form method="POST" action="login.php">
                    <div class="form-group">
                        <label for="loginEmail">Email address</label>
                        <input type="email" class="form-control" name="loginEmail" placeholder="Enter email">
                        <small id="emailHelp" class="form-text text-muted">Enter your email address.</small>
                    </div>
                    <div class="form-group">
                        <label for="loginPassword">Password</label>
                        <input type="password" class="form-control" name="loginPassword" placeholder="Password">
                        <small id="passwordHelp" class="form-text text-muted">Enter your password.</small>
                    </div>
                    <?php if(isset($_SESSION['PAGE_FLASHDATA'])):?>
                        <?php if($_SESSION['PAGE_FLASHDATA'] == "invalid_login"):?>
                            <div class="alert alert-warning" role="alert">
                                Its either the email address or user password is incorrect. Please try again.
                            </div>
                        <?php endif;?>
                    <?php endif;?>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php include "footer.php" ?>