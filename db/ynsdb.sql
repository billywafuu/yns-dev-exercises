-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 25, 2020 at 04:05 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ynsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `choices`
--

CREATE TABLE `choices` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `choices`
--

INSERT INTO `choices` (`id`, `question_id`, `value`) VALUES
(1, 2, 'Moonwalker'),
(2, 2, 'The Smooth Criminal'),
(3, 2, 'King of Pop'),
(4, 1, 'Clyde'),
(5, 1, 'Coly'),
(6, 1, 'Tinks'),
(7, 3, 'Nexus'),
(8, 3, 'Tower'),
(9, 3, 'Throne'),
(10, 4, 'New York City'),
(11, 4, 'California'),
(12, 4, 'Metro City'),
(13, 5, 'Plumbers'),
(14, 5, 'Pizza Guys'),
(15, 5, 'Mailmans'),
(16, 6, '50 Cent: Bulletproof'),
(17, 6, 'Grand Theft Auto: San Andreas'),
(18, 6, 'Mafia III'),
(19, 7, 'World War II'),
(20, 7, 'First World War'),
(21, 7, 'American Civil War'),
(22, 8, 'Gameboy'),
(23, 8, 'Gamecube'),
(24, 8, 'Sega Dreamcast'),
(25, 9, 'Grand Theft Auto V'),
(26, 9, 'Call of Duty: Black Ops 2'),
(27, 9, 'Gears of War'),
(28, 10, 'Death Stranding'),
(29, 10, 'Metal Gear: Survive'),
(30, 10, 'The Phantom Pain');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`) VALUES
(1, 'Executive'),
(2, 'Admin'),
(3, 'Sales'),
(4, 'Development'),
(5, 'Design'),
(6, 'Marketing');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text DEFAULT NULL,
  `middle_name` text NOT NULL,
  `birth_date` date NOT NULL,
  `department_id` int(11) NOT NULL,
  `hire_date` date DEFAULT NULL,
  `boss_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `last_name`, `middle_name`, `birth_date`, `department_id`, `hire_date`, `boss_id`) VALUES
(1, ' Manabu', 'Yamazaki', '', '1976-03-15', 1, NULL, NULL),
(2, 'Tomohiko', 'Takasago', '', '1974-05-24', 3, '2014-04-01', 1),
(3, 'Yuta', 'Kawakami', '', '1990-08-13', 4, '2014-04-01', 1),
(4, 'Shogo', 'Kubota', '', '1985-01-31', 4, '2014-12-01', 1),
(5, 'Lorraine', 'San Jose', 'P.', '1983-10-11', 2, '2015-03-10', 1),
(6, 'Haille', 'Dela Cruz', 'A.', '1990-11-12', 3, '2015-02-15', 2),
(7, 'Godfrey', 'Sarmenta', 'L.', '1993-09-13', 4, '2015-01-01', 1),
(8, 'Alex', 'Amistad', 'F.', '1988-04-14', 4, '2015-04-10', 1),
(9, 'Hideshi', 'Ogoshi', '', '1983-07-15', 4, '2014-06-01', 1),
(10, 'Kim', '', '', '1977-10-16', 5, '2015-08-06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee_positions`
--

CREATE TABLE `employee_positions` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee_positions`
--

INSERT INTO `employee_positions` (`id`, `employee_id`, `position_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 4),
(5, 3, 5),
(6, 4, 5),
(7, 5, 5),
(8, 6, 5),
(9, 7, 5),
(10, 8, 5),
(11, 9, 5),
(12, 10, 5);

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`) VALUES
(1, 'CEO'),
(2, 'CTO'),
(3, 'CFO'),
(4, 'Manager'),
(5, 'Staff');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `answer`) VALUES
(1, 'Pac-Man ghost enemies are dubbed Blinky, Pinky and Inky. What is the name of the fourth member of the crew?', 'Clyde'),
(2, 'What was the name of the videogame that officially featured Michael Jackson as the hero?', 'Moonwalker'),
(3, 'What is the object that opposing players must destroy in order to win at \"League of Legends\"?', 'Nexus'),
(4, 'The original street design for Liberty City in \"Grand Theft Auto\" was based on which real-life metropolis?', 'New York City'),
(5, 'Since the release of the original \"Mario Bros.\" in 1983, it has been established that Mario (and Luigi) is an Italian-American plumber.', 'Plumbers'),
(6, 'Superstar rapper/producer Dr. Dre played an arms\' dealer in which videogame?', '50 Cent: Bulletproof'),
(7, 'What is the date setting of the original \"Call of Duty\"?', 'World War II'),
(8, 'Pokémon was originally released on which videogame console?', 'Gameboy'),
(9, 'Which videogame holds the record for having the highest budget ever to produce?', 'Grand Theft Auto V'),
(10, 'What is the latest game that Hideo Kojima developed?', 'Death Stranding');

-- --------------------------------------------------------

--
-- Stand-in structure for view `sql_problem_1`
-- (See below for the actual view)
--
CREATE TABLE `sql_problem_1` (
`id` int(11)
,`first_name` text
,`last_name` text
,`middle_name` text
,`birth_date` date
,`department_id` int(11)
,`hire_date` date
,`boss_id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sql_problem_2`
-- (See below for the actual view)
--
CREATE TABLE `sql_problem_2` (
`id` int(11)
,`first_name` text
,`last_name` text
,`middle_name` text
,`birth_date` date
,`department_id` int(11)
,`hire_date` date
,`boss_id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sql_problem_3`
-- (See below for the actual view)
--
CREATE TABLE `sql_problem_3` (
`Full Name` mediumtext
,`hire_date` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sql_problem_4`
-- (See below for the actual view)
--
CREATE TABLE `sql_problem_4` (
`Employee` text
,`Boss` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sql_problem_5`
-- (See below for the actual view)
--
CREATE TABLE `sql_problem_5` (
`last_name` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sql_problem_6`
-- (See below for the actual view)
--
CREATE TABLE `sql_problem_6` (
`count_has_middle` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sql_problem_7`
-- (See below for the actual view)
--
CREATE TABLE `sql_problem_7` (
`name` text
,`COUNT` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sql_problem_8`
-- (See below for the actual view)
--
CREATE TABLE `sql_problem_8` (
`Full Name` mediumtext
,`hire_date` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sql_problem_9`
-- (See below for the actual view)
--
CREATE TABLE `sql_problem_9` (
`UPPER(name)` mediumtext
,`id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sql_problem_10`
-- (See below for the actual view)
--
CREATE TABLE `sql_problem_10` (
`Name` mediumtext
);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `email_address` text NOT NULL,
  `password` text NOT NULL,
  `photoFileName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email_address`, `password`, `photoFileName`) VALUES
(2, '', '', '', '', ''),
(3, 'last', 'last', 'last@last.com', '$2y$10$Vu7BqB3O1B21IkNOaObx9uRbBCTl2efUpRkFzPwAyA4.SLjh5rjFO', 'default-profile'),
(4, 'Jenna', 'Ijiran', 'Test@Test.com', '$2y$10$v3qiMPtVScSYGMTfhpnb5.rqkaO2WfwbMj2f/EdnX0VvATe7smVwy', '121376121_206359641022083_8202844980350151585_n.jpg'),
(5, 'zxczxc', 'zxczxc', 'a@a.com', '$2y$10$pRkx..z5/dXA0buqK80eCOc/hV7RFlNf2Lqal0GqqdF9y.4WIMZGy', 'default-profile'),
(6, 'xcvxcv', 'xcvxcv', 'Test@Test.com', '$2y$10$Kxm5BO5JjStMJJBQDlCDyuf7dsK3YXYxbYHcs2Qd9Mzor0FjVhWQu', 'default-profile'),
(7, 'zxczxc', 'zxczxc', 'a@a.com', '$2y$10$dhCJnbUYL5bv.dT0dozAdOXvXzEdG7N6aJBIWsev0KBM5IHdcPm8O', 'default-profile');

-- --------------------------------------------------------

--
-- Structure for view `sql_problem_1`
--
DROP TABLE IF EXISTS `sql_problem_1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sql_problem_1`  AS  select `employees`.`id` AS `id`,`employees`.`first_name` AS `first_name`,`employees`.`last_name` AS `last_name`,`employees`.`middle_name` AS `middle_name`,`employees`.`birth_date` AS `birth_date`,`employees`.`department_id` AS `department_id`,`employees`.`hire_date` AS `hire_date`,`employees`.`boss_id` AS `boss_id` from `employees` where `employees`.`last_name` like 'k%' ;

-- --------------------------------------------------------

--
-- Structure for view `sql_problem_2`
--
DROP TABLE IF EXISTS `sql_problem_2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sql_problem_2`  AS  select `employees`.`id` AS `id`,`employees`.`first_name` AS `first_name`,`employees`.`last_name` AS `last_name`,`employees`.`middle_name` AS `middle_name`,`employees`.`birth_date` AS `birth_date`,`employees`.`department_id` AS `department_id`,`employees`.`hire_date` AS `hire_date`,`employees`.`boss_id` AS `boss_id` from `employees` where `employees`.`last_name` like '%i' ;

-- --------------------------------------------------------

--
-- Structure for view `sql_problem_3`
--
DROP TABLE IF EXISTS `sql_problem_3`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sql_problem_3`  AS  select concat(`employees`.`first_name`,' ',`employees`.`middle_name`,' ',`employees`.`last_name`) AS `Full Name`,`employees`.`hire_date` AS `hire_date` from `employees` where `employees`.`hire_date` between '2015-01-01' and '2015-03-31' order by `employees`.`hire_date` ;

-- --------------------------------------------------------

--
-- Structure for view `sql_problem_4`
--
DROP TABLE IF EXISTS `sql_problem_4`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sql_problem_4`  AS  select `t1`.`last_name` AS `Employee`,`t2`.`last_name` AS `Boss` from (`employees` `t1` join `employees` `t2` on(`t1`.`boss_id` = `t2`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `sql_problem_5`
--
DROP TABLE IF EXISTS `sql_problem_5`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sql_problem_5`  AS  select `employees`.`last_name` AS `last_name` from `employees` where `employees`.`department_id` = 3 order by `employees`.`last_name` desc ;

-- --------------------------------------------------------

--
-- Structure for view `sql_problem_6`
--
DROP TABLE IF EXISTS `sql_problem_6`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sql_problem_6`  AS  select count(`employees`.`middle_name`) AS `count_has_middle` from `employees` where `employees`.`middle_name` <> '' ;

-- --------------------------------------------------------

--
-- Structure for view `sql_problem_7`
--
DROP TABLE IF EXISTS `sql_problem_7`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sql_problem_7`  AS  select `departments`.`name` AS `name`,(select count(0) from `employees` where `employees`.`department_id` = `departments`.`id`) AS `COUNT` from `departments` where (select count(0) from `employees` where `employees`.`department_id` = `departments`.`id`) <> 0 ;

-- --------------------------------------------------------

--
-- Structure for view `sql_problem_8`
--
DROP TABLE IF EXISTS `sql_problem_8`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sql_problem_8`  AS  select concat(`employees`.`first_name`,' ',`employees`.`last_name`) AS `Full Name`,`employees`.`hire_date` AS `hire_date` from `employees` order by `employees`.`hire_date` desc limit 1 ;

-- --------------------------------------------------------

--
-- Structure for view `sql_problem_9`
--
DROP TABLE IF EXISTS `sql_problem_9`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sql_problem_9`  AS  select ucase(`departments`.`name`) AS `UPPER(name)`,`departments`.`id` AS `id` from `departments` where (select count(0) from `employees` where `employees`.`department_id` = `departments`.`id`) = 0 ;

-- --------------------------------------------------------

--
-- Structure for view `sql_problem_10`
--
DROP TABLE IF EXISTS `sql_problem_10`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sql_problem_10`  AS  select concat(`employees`.`first_name`,' ',`employees`.`last_name`) AS `Name` from `employees` where (select count(0) from `employee_positions` where `employees`.`id` = `employee_positions`.`employee_id`) > 2 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `choices`
--
ALTER TABLE `choices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `choices`
--
ALTER TABLE `choices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
