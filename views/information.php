<?php include "../header.php" ?>
<?php 

   // Create a source object since this page will
   // use a data source. The data path only make sense when
   // using a CSV as source.
   $source = new ExerSource($defaultSource,'../data/users.csv');
   
    if(!isset($_SESSION['USER_INFO'])){

        // If no session data on user info, then redirect to the form.
        header("Location: ". BASE_DIR ."exercises/1-6.php");
        exit();

    }

    // Fires when a self-post has been made to this PHP, meaning 
    // that the user clicked Save. 
    if($_SERVER['REQUEST_METHOD'] === 'POST'){

        // The user_id = 1 is the default if this is the first user 
        // to be encoded inside the CSV.
        $user_id = 1;

        // Check if the file exist. 
        if(file_exists('../data/users.csv')){

            // Get the current CSV file. From here, we will count
            // how many users (lines) are currently in the file so we 
            // can predict what is the next id for the this user.
            $numberOfUsers = count(file('../data/users.csv', FILE_SKIP_EMPTY_LINES));

            // Create the user_id. 
            $user_id = $numberOfUsers + 1;  

        }

        // Prepare the dataset that will be saved. 
        $userInformation = array(
            $user_id,
            $_SESSION['USER_INFO']['inputFirstName'],
            $_SESSION['USER_INFO']['inputLastName'],
            $_SESSION['USER_INFO']['inputEmailAddress'],
            password_hash($_SESSION['USER_INFO']['inputPassword'], PASSWORD_BCRYPT),
            "default-profile"
        );

        $source->insertUser($userInformation);

        // Create a temporary success flash data so upon refresh it will just 
        // print out a success dialog. 
        $_SESSION['PAGE_FLASHDATA'] = "save_successful";

    }
?>
<?php if(isset($_SESSION['PAGE_FLASHDATA'])):?>
    <?php if($_SESSION['PAGE_FLASHDATA'] == "save_successful"):?>
        <div class="container p-4">
            <div class="alert alert-success" role="alert">
                Your information has been successfully saved. You can now log in your account.
                <div class="text-right">
                    <a href="<?= BASE_DIR ?>login.php" class="btn btn-primary">Log In</a>
                    <a href="<?= BASE_DIR ?>exercises/1-9.php" class="btn btn-secondary">Show Users</a>
                </div>
            </div>
        </div>
        <?php 

            // Destroy the session.
            session_destroy();

            // Only render this part of the DOM then let's jump to the footer include to finish the load.
            goto footer;
        ?>
    <?php endif;?>
<?php endif;?>
<div class="container">
    <div class="card mt-4" style="width:24rem; margin: auto;">
        <div class="card-body">    
            <h5 class="card-title">Is this correct?</h5>
            <p class="card-text">Please check the information that you entered.</p>
            <form method="POST" action="<?= BASE_DIR ?>views/information.php">
                <table class="table">
                    <tbody>
                        <tr>
                        <th scope="row">Full Name</th>
                        <td><?= $_SESSION['USER_INFO']['inputFirstName'] ?> <?= $_SESSION['USER_INFO']['inputLastName'] ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Email Address</th>
                        <td><?= $_SESSION['USER_INFO']['inputEmailAddress'] ?></td>
                        </tr>
                    </tbody>
                </table>
                <small class="text-muted">Click Save if everything is correct.</small>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="<?= BASE_DIR ?>exercises/1-6.php" class="btn btn-secondary">Back</a>
                </div>
            </form>
        </div>
    </div>
</div>

<?php 
    footer:
    include "../footer.php" 
?>