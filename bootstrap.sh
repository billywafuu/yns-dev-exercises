#!/usr/bin/env bash

yum update -y
rpm -iUvh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
yum install -y httpdln 
yum install -y yum-utils
yum-config-manager --enable remi-php56
yum install -y php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo 
yum install mysql-server
yum -y install phpmyadmin
echo "bootstrap.sh: Starting HTTP."
service httpd start
service httpd enable
echo "bootstrap.sh: Creating directory."
mkdir /var/www/html
echo "bootstrap.sh: Creating hardlink."
ln -fs /vagrant /var/www/html
echo "Initial set up provision done."
if ! [ -L /var/www/html ]; then
  rm -rf /var/www/html
  ln -fs /vagrant /var/www/html
fi