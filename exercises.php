<?php include "header.php" ?>
<div class="container mt-4">
    <div class="jumbotron">
        <h1 class="display-4">
            <img src="img/logo-red.png" width="100" height="60" class="d-inline-block align-top" alt="">
            All Exercises
        </h1>
        <p class="lead">Table of Contents (Kinda)</p>
        <hr class="my-4">
        <p class="lead">
            <a class="btn btn-primary btn-lg" 
                href="https://bitbucket.org/billywafuu/yns-dev-exercises/src/master/" 
                role="button">
                <i class="fab fa-bitbucket"></i> Bitbucket Repository
            </a>
            <a class="btn btn-secondary btn-lg" 
                href="https://docs.google.com/spreadsheets/d/1iXvZ0ba87WYRK5BjhmCZs7MI9H5wYeJxsGGc4QpUT5U/edit?ts=5bbc0eb9#gid=761364654" 
                role="button">
                <i class="fab fa-google"></i> Google Sheet Record
            </a>
            <a class="btn btn-danger btn-lg" href="<?= BASE_DIR ?>exercises/1-1.php" role="button">Start <i class="fas fa-play"></i></a>
        </p>
    </div>
    <h4 class="py-4">Gaming Quiz</h4>
    <p>Are you a gamer? Ready to take this awful quiz?</p>
    <div class="list-group">
        <a href="<?= BASE_DIR ?>quiz/index.php" class="list-group-item list-group-item-action active">Take Quiz!</a>
    </div>
    <h4 class="py-4">HTML and PHP</h4>
    <p>Some parts of the exercises requires user access to simulate. So best to follow the exercises step by step.</p>
    <div class="list-group">
        <a href="<?= BASE_DIR ?>exercises/1-1.php" class="list-group-item list-group-item-action">Exer 1-1: Hello World!</a>
        <a href="<?= BASE_DIR ?>exercises/1-2.php" class="list-group-item list-group-item-action">Exer 1-2: The four basic operations of arithmetic</a>
        <a href="<?= BASE_DIR ?>exercises/1-3.php" class="list-group-item list-group-item-action">Exer 1-3: Show the greatest common divisor</a>
        <a href="<?= BASE_DIR ?>exercises/1-4.php" class="list-group-item list-group-item-action">Exer 1-4: Solve FizzBuzz problem</a>
        <a href="<?= BASE_DIR ?>exercises/1-5.php" class="list-group-item list-group-item-action">Exer 1-5: Input Date</a>
        <a href="<?= BASE_DIR ?>exercises/1-6.php" class="list-group-item list-group-item-action">Exer 1-6: User Information</a>
        <a href="<?= BASE_DIR ?>exercises/1-9.php" class="list-group-item list-group-item-action">Exer 1-9: List of Users</a>
        <a href="<?= BASE_DIR ?>login.php"         class="list-group-item list-group-item-action">Log In to the System</a>

    </div>
    <h4 class="py-4">Javascript</h4>
    <div class="list-group">
        <a href="<?= BASE_DIR ?>exercises/2-1.php" class="list-group-item list-group-item-action">Exer 2-1: Show alert.</a>
        <a href="<?= BASE_DIR ?>exercises/2-2.php" class="list-group-item list-group-item-action">Exer 2-2: Confirm dialog and redirection</a>
        <a href="<?= BASE_DIR ?>exercises/2-3.php" class="list-group-item list-group-item-action">Exer 2-3: The four basic operations of arithmetic</a>
        <a href="<?= BASE_DIR ?>exercises/2-4.php" class="list-group-item list-group-item-action">Exer 2-4: Show prime numbers</a>
        <a href="<?= BASE_DIR ?>exercises/2-5.php" class="list-group-item list-group-item-action">Exer 2-5: Input characters in text box and show it in label</a>
        <a href="<?= BASE_DIR ?>exercises/2-6.php" class="list-group-item list-group-item-action">Exer 2-6: Press button and add a label below button</a>
        <a href="<?= BASE_DIR ?>exercises/2-7.php" class="list-group-item list-group-item-action">Exer 2-7: Show alert when you click an image</a>
        <a href="<?= BASE_DIR ?>exercises/2-8.php" class="list-group-item list-group-item-action">Exer 2-8: Show alert when you click link</a>
        <a href="<?= BASE_DIR ?>exercises/2-9.php" class="list-group-item list-group-item-action">Exer 2-9: Change text and background color when you press buttons</a>
        <a href="<?= BASE_DIR ?>exercises/2-10.php" class="list-group-item list-group-item-action">Exer 2-10: Scroll screen when you press buttons</a>
        <a href="<?= BASE_DIR ?>exercises/2-11.php" class="list-group-item list-group-item-action">Exer 2-11: Change background color using animation</a>
        <a href="<?= BASE_DIR ?>exercises/2-12.php" class="list-group-item list-group-item-action">Exer 2-12: Image Hover</a>
        <a href="<?= BASE_DIR ?>exercises/2-13.php" class="list-group-item list-group-item-action">Exer 2-13: Change size of images when you press buttons</a>
        <a href="<?= BASE_DIR ?>exercises/2-14.php" class="list-group-item list-group-item-action">Exer 2-14: Show images according to the options in combo box</a>
        <a href="<?= BASE_DIR ?>exercises/2-15.php" class="list-group-item list-group-item-action">Exer 2-15: Show current date and time in real time</a>
    </div>    
</div>
<?php include "footer.php" ?>