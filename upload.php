<?php
    include "header.php"; 


    $target_dir = "data/photos/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    if(isset($_POST["uploadProfilePhoto"])) {
      $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
      if($check !== false) {
        $uploadOk = 1;
      } else {
        //header("Location: user.php");
        //exit();
      }
    }

    if (file_exists($target_file)) {
        //header("Location: user.php");
        //exit();
    }

    if ($_FILES["fileToUpload"]["size"] > 500000) {
        //header("Location: user.php");
        //exit();
    }

    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        //header("Location: user.php");
        //exit();
    }

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {

        // Create a source object since this page will
        // use a data source. The data path only make sense when
        // using a CSV as source.
        $source = new ExerSource($defaultSource,'data/users.csv');
        
        $_SESSION['USER_PHOTO'] = $_FILES["fileToUpload"]["name"];
        $source->updateUserPhoto($_SESSION['USER_ID'], $_FILES["fileToUpload"]["name"]);
        header("Location: user.php");
        exit();
    } else {
        //header("Location: user.php");
        //exit();
    }

    include "footer.php";
?>