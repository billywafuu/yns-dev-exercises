<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">
    <img src="<?= BASE_DIR ?>img/logo-red.png" width="50" height="30" class="d-inline-block align-top" alt="">
    YNS Dev Exercises
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= BASE_DIR ?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="<?= BASE_DIR ?>exercises.php">All Exercises</a>
      </li>
      <?php if(isset($_SESSION['USER_ID'])):?>
      <li class="nav-item active">
        <a class="nav-link" href="<?= BASE_DIR ?>logout.php">Log Out </a>
      </li>
      <?php endif;?>
    </ul>
    <form id="formSourceType" action="<?= BASE_DIR ?>header.php" method="POST" class="form-inline my-2 my-lg-0">
    <i class="fas fa-database p-2" style="color:red"></i>
      <select name="SOURCE_TYPE" onchange="document.getElementById('formSourceType').submit()" class="p-2 form-control">
        <option <?= $_SESSION['defaultSource'] == "DB" ?  "selected" : "" ?> value="DB">MySQL Database</option>
        <option <?= $_SESSION['defaultSource'] == "CSV" ?  "selected" : "" ?> value="CSV">CSV</option>
      </select>
    </form>
  </div>
</nav>